<?php namespace Tsawler\McaPackage;

use App\Events\RecordPageViewEvent;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;


/**
 * Class OpenHouseController
 * @package Tsawler\McaPackage
 */
class OpenHouseController extends Controller
{

    /**
     * @param Request $request
     * @return mixed
     */
    public function postOpenHouse(Request $request)
    {
        if (env('APP_DEBUG')) {
            $this->validate($request, [
                'first_name'      => 'required',
                'last_name'       => 'required',
                'email'           => 'email|required',
                'phone'           => 'required',
                'grade'           => 'required',
                'year'            => 'required',
                'date'            => 'required',
            ]);
        } else {
            $this->validate($request, [
                'first_name'           => 'required',
                'last_name'            => 'required',
                'email'                => 'email|required',
                'phone'                => 'required',
                'grade'                => 'required',
                'year'                 => 'required',
                'date'                 => 'required',
                'g-recaptcha-response' => 'required|captcha',
            ]);
        }

        $data = [
            'first_name'      => Input::get('first_name'),
            'last_name'       => Input::get('last_name'),
            'phone'           => Input::get('phone'),
            'phone_alternate' => Input::get('phone_alternate'),
            'email'           => Input::get('email'),
            'email_alternate' => Input::get('email_alternate'),
            'grade'           => Input::get('grade'),
            'year'            => Input::get('year'),
            'date'            => Input::get('date'),
        ];

        Mail::queue(new OpenHouseMailable($data));

        event(new RecordPageViewEvent(\Illuminate\Support\Facades\Request::ip(), \Illuminate\Support\Facades\Request::url(), "Open House Form filled out"));

        // give reponse
        return Redirect::to('/open-house-thanks');

    }
}
