<?php namespace Tsawler\McaPackage;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Support\ServiceProvider;

/**
 * Class McaPackageServiceProvider
 * @package Tsawler\McaPackage;
 */
class McaPackageServiceProvider extends ServiceProvider
{

    /**
     * Bootstrap the application services.
     *c
     * @return void
     */
    public function boot()
    {
        $this->loadRoutesFrom(__DIR__ . '/web.php');
        $this->loadViewsFrom(__DIR__ . '/views', 'mcapackage');
    }


    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {

    }

}
