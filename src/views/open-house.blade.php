@component('mail::message')
# Open House Registration Received

The following registration came in from the website:

First name: {{ $data['first_name'] }}

Last Name: {{ $data['last_name'] }}

Email: {{ $data['email'] }}

Alternate Email: {{ $data['email_alternate'] }}

Phone: {{ $data['phone'] }}

Alternate Phone: {{ $data['phone_alternate'] }}

Grade: {{ $data['grade'] }}

Year: {{ $data['year'] }}

Date: {{ $data['date'] }}

@endcomponent
