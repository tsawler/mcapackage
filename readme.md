# MCA Package

## Use this in production

* Add the repository to composer.json:


~~~~
"repositories": [
    {
      "type": "vcs",
      "url": "git@bitbucket.org:tsawler/mcapackage.git"
    }
    ],
    "require": {
~~~~

* Add the package in composer.json

~~~~
    "torann/geoip": "^1.0",
    "tsawler/mcapackage": "dev-master",
    "unisharp/laravel-filemanager": "^1.8",
~~~~

* run composer update

## Setup Instructions for development

1. Install a new [ Blender ](https://bitbucket.org/tsawler/blender) app
2. Clone this package
3. Put contents of this package into laravel app at this location: <root>/packages/tsawler/mcapackage
4. Add this the the app's composer.json file:

~~~~
"autoload": {
    "classmap": [
        "database/seeds",
        "database/factories"
    ],
    "psr-4": {
        "App\\": "app/",
        "Tsawler\\McaPackage\\": "packages/tsawler/mcapackage/src"
    }
},
~~~~

_For development only_, you need to add this to config/app.php:

~~~~
   Tsawler\McaPackage\McaPackageServiceProvider::class,
~~~~


Run composer update


